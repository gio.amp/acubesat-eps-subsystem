The hardware that I designed while working on the AcubeSAT nanosatellite project: https://gitlab.com/acubesat

Notice: The physical board's digital 3.3V power line, proved to have too much inductance and the STM32F4-series MCU could not be programmed. Also check the MCU's decoupling capacitors' value.